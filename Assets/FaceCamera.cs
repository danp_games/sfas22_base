using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceCamera : MonoBehaviour
{
    private GameObject _cam;

    void Start()
    {
        _cam = ModeManager._playerCamRef.gameObject;
        
    }
    // Update is called once per frame
    void Update()
    {
        transform.LookAt(_cam.transform);
    }
}

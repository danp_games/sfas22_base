using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoonManager : MonoBehaviour
{

    // Swift Boons
    public void SwiftSpeed()
    {
        Player.MovementSpeedMulti += 0.1f;
    }
    public void SwiftReload()
    {
        Weapon.ReloadMulti = Weapon.ReloadMulti - 0.1f < 0.5f ? 0.5f : Weapon.ReloadMulti - 0.1f;
    }

    // War Boons
    public void WarDamage()
    {
        Weapon.DamageMulti += 0.1f;
    }
    public void WarRoF()
    {
        Weapon.RoFMulti += 0.1f;
    }

    // Defender Boons
    public void DefenderHealth()
    {
        Player.HealthMulti += 0.1f;
    }
    public void DefenderRegen()
    {
        Player.HealthRegenRateMulti = Player.HealthRegenRateMulti - 0.1f < 0.5f ? 0.5f : Player.HealthRegenRateMulti - 0.1f;
    }
}

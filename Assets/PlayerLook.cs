using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerLook : MonoBehaviour
{
    private const float MouseSensitivity = 0.1f;

    private float xRotation = 0f;

    private Transform _playerBody;

    // Start is called before the first frame update
    void Start()
    {
        _playerBody = transform.parent.gameObject.GetComponent<Transform>();
        //Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        var mouse = Mouse.current;
        float mouseX = mouse.delta.x.ReadValue() * MouseSensitivity;
        float mouseY = mouse.delta.y.ReadValue() * MouseSensitivity;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);

        _playerBody.Rotate(Vector3.up * mouseX);
    }
}

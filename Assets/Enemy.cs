
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{

    private float _health = 100;
    private float _maxHealth = 100;
    [SerializeField] private float _attackRange = 100;
    [SerializeField] private float _attackDamage = 5;
    [SerializeField] private float _attackDelay = 5;
    private float _attackTimer;
    private bool _isCloseToPlayer;
    private Animator _animator;
    private Slider _slider;
    private NavMeshAgent _agent;
    private GameObject _playerRef;
    private ModeManager _modeRef;
    private float _initialSpeed;
    void Start()
    {
        _attackTimer = 0;
        _animator = GetComponent<Animator>();
        _agent = GetComponent<NavMeshAgent>();
        _initialSpeed = _agent.speed;
        _playerRef = GameObject.FindGameObjectWithTag("Player");
        _modeRef = GameObject.FindGameObjectWithTag("ModeManager").GetComponent<ModeManager>();
        _maxHealth *= ModeManager._enemyHealthMulti;
        _health = _maxHealth;
        _slider = transform.GetChild(0).transform.GetChild(0).gameObject.GetComponent<Slider>();
    }
    // Update is called once per frame
    void Update()
    {
        _agent.speed = _initialSpeed * ModeManager._enemySpeedMulti;
        _agent.destination = _playerRef.transform.position;
        _slider.value = _health / _maxHealth;
        _isCloseToPlayer = CalculateDistance(transform, _playerRef.transform) < _attackRange;
        _animator.SetBool("Attacking", _isCloseToPlayer);

        if (_attackTimer > 0.0f)
        {
            _attackTimer -= Time.deltaTime;
        }

        if (_isCloseToPlayer && _attackTimer <= 0.0f)
        {
            _attackTimer = _attackDelay;
            _playerRef.GetComponent<Player>().DamagePlayer(_attackDamage);
        }
    }

    private static float CalculateDistance(Transform a, Transform b)
    {
        return Mathf.Sqrt(Mathf.Pow((b.position.x - a.position.x), 2) +
                          Mathf.Pow((b.position.y - a.position.y), 2) +
                          Mathf.Pow((b.position.z - a.position.z), 2));
    }

    public void DamageEnemy(float damageValue)
    {
        _health -= damageValue;
        if (_health <= 0.0f)
        {
            _health = 0;
            _slider.value = _health;
            _modeRef.EnemyCount--;
            _modeRef._noOfEnemiesToKill--;
            Destroy(gameObject);
        }
    }
}

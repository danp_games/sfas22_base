using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public enum Mode
{
    Eliminate,Defend
};



public class ModeManager : MonoBehaviour
{
    // STATICS
    [Header("Multipliers")]
    [SerializeField] internal static float _enemySpeedMulti = 1.0f;
    [SerializeField] internal static float _enemyHealthMulti = 1.0f;
    private static Mode _mode = Mode.Eliminate;

    internal static bool Playing = false;
    // STATICS

    internal int EnemyCount;

    [SerializeField] private GameObject _enemyPrefab;
    [SerializeField] private TMPro.TextMeshProUGUI _enemyText;
    [SerializeField] internal GameObject _boonUI;


    [Header("Mode Values")]
    [SerializeField] internal int _noOfEnemiesToKill = 1;
    [SerializeField] private float _spawnDelay = 1.0f;
    private int _maxEnemies = 2;

    [SerializeField] internal static Camera _playerCamRef;
    [SerializeField] internal static Camera _mainCamRef;
    [SerializeField] internal static GameObject _playerCanvasRef;
    [SerializeField] internal static GameObject _mainCanvasRef;


    //[SerializeField] internal float _enemyHealthMulti = 1.0f;

    internal bool _isComplete = false;
    internal bool _boonChosen = false;
    private float _spawnDelayTimer;
    private readonly List<GameObject> _enemySpawnLocations = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {

        _mainCamRef = GameObject.FindGameObjectWithTag("MainCamera").gameObject.GetComponent<Camera>();
        _playerCamRef = GameObject.FindGameObjectWithTag("PlayerCamera").gameObject.GetComponent<Camera>();
        _mainCanvasRef = GameObject.FindGameObjectWithTag("MainCanvas").gameObject;
        _playerCanvasRef = GameObject.FindGameObjectWithTag("PlayerCanvas").gameObject;
        
        _mainCamRef.enabled = true;
        _playerCamRef.enabled = false;
        _mainCanvasRef.SetActive(true);
        _playerCanvasRef.SetActive(false);
        _boonUI.SetActive(false);
        Player._paused = true;
        Random.InitState((int) DateTime.Now.Ticks);
        GameObject[] locations = GameObject.FindGameObjectsWithTag("EnemySpawnLoc");
        foreach (var loc in locations)
        {
            _enemySpawnLocations.Add(loc);   
        }

        _spawnDelayTimer = _spawnDelay;
        Cursor.lockState = CursorLockMode.None;
    }

    // Update is called once per frame
    void Update()
    {
        if (Playing)
        {
            _enemyText.text = "Enemies Remaining: " + _noOfEnemiesToKill;
            if (_noOfEnemiesToKill < 1 && !_isComplete)
            {
                _isComplete = true;
                _boonUI.SetActive(true);
                Cursor.lockState = CursorLockMode.None;
                Player._paused = true;
            }

            if (!_isComplete)
            {
                switch (_mode)
                {
                    case (Mode.Eliminate):
                    {
                        _spawnDelayTimer -= Time.deltaTime;
                        _spawnDelayTimer = _spawnDelayTimer < 0 ? 0 : _spawnDelayTimer;

                        if (EnemyCount < _maxEnemies && _spawnDelayTimer == 0.0f && _noOfEnemiesToKill > EnemyCount)
                        {
                            _spawnDelayTimer = _spawnDelay;
                            var randomSpawnLoc = Random.Range(0, _enemySpawnLocations.Count);
                            _enemySpawnLocations[randomSpawnLoc].GetComponent<SpawnLocGizmos>().ParticleSystem.Play();
                            StartCoroutine(Spawn(randomSpawnLoc));
                        }

                        EnemyCount = EnemyCount < 0 ? 0 : EnemyCount;
                        break;
                    }
                    case (Mode.Defend):
                    {
                        break;
                    }
                }
            }
        }
    }

    public void Reset()
    {
        _noOfEnemiesToKill = 1;
        _isComplete = false;
        _boonUI.transform.GetChild(0).GetChild(1).gameObject.GetComponent<Button>().interactable = true;
        _boonUI.transform.GetChild(0).GetChild(2).gameObject.GetComponent<Button>().interactable = true;
        _boonUI.transform.GetChild(0).GetChild(3).gameObject.GetComponent<Button>().interactable = true;
    }

    public void GoBackToMenu()
    {
        StartCoroutine(BackToMenu());
    }

    private IEnumerator BackToMenu()
    {
        yield return new WaitForSeconds(1.0f);
        Board._checked = false;
        Player._paused = true;
        Game._encounterLevel++;
        Game._clicked = false;
        _mainCamRef.enabled = true;
        _playerCamRef.enabled = false;
        _mainCanvasRef.SetActive(true);
        _playerCanvasRef.SetActive(false);
        Playing = false;
        Debug.Log("Lets go!");
    }
    private IEnumerator Spawn(int randomSpawnLoc)
    {
        yield return new WaitForSeconds(1.0f);
        Instantiate(_enemyPrefab, _enemySpawnLocations[randomSpawnLoc].transform.position, _enemySpawnLocations[randomSpawnLoc].transform.rotation);
        Debug.Log("Spawned");
        EnemyCount++;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayerMarker : MonoBehaviour
{
    [SerializeField] private Board _boardRef;

    void Start()
    {
        GetComponent<Camera>().orthographicSize = 2;
    }

    // Update is called once per frame
    void Update()
    {
        Box plr = _boardRef.FindBoxWithPosition(Board.PlayerPosition);
        Vector3 plrTransform = new Vector3(plr.transform.position.x, transform.position.y, plr.transform.position.z);
        transform.position = Vector3.Lerp(transform.position, plrTransform, Time.deltaTime * 5);
    }
}
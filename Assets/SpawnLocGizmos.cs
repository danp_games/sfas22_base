using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnLocGizmos : MonoBehaviour
{
    [SerializeField] private Mesh _mesh;

    public ParticleSystem ParticleSystem;
    // Start is called before the first frame update
    void Start()
    {
        ParticleSystem = transform.GetChild(0).gameObject.GetComponentInChildren<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnDrawGizmos()
    {
        var color = Color.cyan;
        color.a = 0.5f;
        Gizmos.color = color;
        
        Gizmos.DrawMesh(_mesh, transform.position, transform.rotation * Quaternion.Euler(90,0,0));
    }
}

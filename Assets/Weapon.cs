using System;
using UnityEngine;
using UnityEngine.UI;

public enum WeaponType
{
    Pistol, Rifle, Heavy, Sniper
};

public class Weapon : MonoBehaviour
{
    internal static float DamageMulti = 1.0f;
    internal static float ReloadMulti = 1.0f;
    internal static float RoFMulti = 1.0f; 
    internal static float CritMulti = 1.0f;

    [SerializeField] private float Damage = 10.0f;
    [SerializeField] private float CriticalBonus = 5.0f;
    [SerializeField] private float Range = 100.0f;

    [SerializeField] private float _reloadTime = 2.0f;
    [SerializeField] internal int _maxAmmo = 8;
    [SerializeField] internal bool FullAuto = false;
    [SerializeField] internal WeaponType _weaponType;
    internal int _currentAmmo = 8;


    // How long in seconds until player can fire after already firing
    [SerializeField] internal float FireRate = 0.25f;
    
    [SerializeField] private GameObject _impactEffect;
    [SerializeField] private GameObject _ammoText;
    [SerializeField] private Image _reloadRing;

    internal Animator _animator;
    private Camera _cameraRef;
    private Player _playerRef;

    internal bool Reloading;
    private float _reloadTimer;
    void Start()
    {
        Reloading = false;
        _reloadTimer = _reloadTime * ReloadMulti;
        _currentAmmo = _maxAmmo;
        _cameraRef = GetComponentInParent<Camera>();
        _playerRef = transform.parent.gameObject.GetComponentInParent<Player>();
        _animator = GetComponent<Animator>();
    }

    public void SwappedTo()
    {
        Reloading = false;
        _reloadTimer = _reloadTime * ReloadMulti;
        _currentAmmo = _maxAmmo;
        _cameraRef = GetComponentInParent<Camera>();
        _playerRef = transform.parent.gameObject.GetComponentInParent<Player>();
        _animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (_currentAmmo <= 0)
        {
            _animator.SetBool("Reloading", true);
            _ammoText.GetComponent<TMPro.TextMeshProUGUI>().text = "Recharging...";
            if (Math.Abs(_reloadTimer - (_reloadTime* ReloadMulti)) < 0.01f)
            {
                _playerRef._sources[1].Play();
            }
            _reloadTimer -= Time.deltaTime;
            _reloadRing.fillAmount = ((_reloadTime* ReloadMulti) - _reloadTimer) / (_reloadTime* ReloadMulti);
            if (_reloadTimer < 0.0f)
            {
                //Reloaded!
                _reloadTimer = _reloadTime* ReloadMulti;
                _currentAmmo = _maxAmmo;
                _reloadRing.fillAmount = 0.0f;
                Reloading = false;
            }
        }
        else
        {
            _animator.SetBool("Reloading", false);
            _ammoText.GetComponent<TMPro.TextMeshProUGUI>().text = _currentAmmo.ToString();
        }

        if (Player._moveRight || Player._moveLeft || Player._moveForward || Player._moveBackward)
        {
            // Player is moving, so update animation

            // Make sure backwards reverses the anim
            //_animator. = _playerRef._moveBackward ? -1.0f : 1.0f;

            _animator.SetBool("Moving", true);
        }
        else
        {
            _animator.SetBool("Moving", false);
        }
    }

    public bool Fire()
    {
        if (!Reloading)
        {
            if (_currentAmmo <= 0)
            {
                Reloading = true;
                return false;
            }
            else
            {
                _currentAmmo--;
            }
            
            if (Physics.Raycast(_cameraRef.transform.position, _cameraRef.transform.forward, out var hit, Range))
            {
                if (hit.transform.gameObject.GetComponent<Enemy>())
                {
                    hit.transform.gameObject.GetComponent<Enemy>().DamageEnemy(Damage* DamageMulti);
                }
                else if (hit.transform.name == "Critical")
                {
                    hit.transform.parent.gameObject.GetComponent<Enemy>().DamageEnemy((Damage* DamageMulti) + (CriticalBonus* CritMulti));
                    Debug.Log("Critical hit!");
                }
                else
                {
                    GameObject impact = Instantiate(_impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
                    impact.transform.parent = hit.transform;
                    Destroy(impact, 2f);
                }
            }


        }

        return true;
    }

}

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    internal static float HealthMulti = 1.0f;
    internal static float MovementSpeedMulti = 1.0f;
    internal static float HealthRegenRateMulti = 1.0f;

    internal static bool _moveForward;
    internal static bool _moveBackward;
    internal static bool _moveLeft;
    internal static bool _moveRight;
    private bool _jump;
    private bool _shoot;
    private bool _reload;
    private bool _firing;
    private bool _swap;
    private CharacterController _characterController;
    private float _gravity = -9.81f;
    private Slider _healthBar;
    private float _health;
    private float _jumpHeight = 1.0f;
    private float _maxHealth = 100.0f;
    private float _speed = 10.0f;
    private float _regenRate = 1.0f;
    private  float _regenSpeed = 30.0f;
    private float _regenTimer;
    private Weapon _gunObject;
    private Vector3 _velocity;
    private ParticleSystem _particleSystem;
    private float _shootTimer;
    internal List<AudioSource> _sources;
    internal static bool _paused = false;
    private float _swapTimer = 1.0f;
    [SerializeField] private GameObject _playerHealthBar;
    internal readonly Vector3 _startPos = new Vector3(0,1.2f,0);
    // Start is called before the first frame update
    void Start()
    {
        _regenTimer = 3.0f;
        _health = _maxHealth * HealthMulti;
        _healthBar = _playerHealthBar.GetComponent<Slider>();
        _characterController = GetComponent<CharacterController>();
        _gunObject = transform.GetChild(0).GetChild(0).gameObject.GetComponent<Weapon>();
        _particleSystem = _gunObject.transform.GetChild(0).gameObject.GetComponent<ParticleSystem>();
        _sources = new List<AudioSource>();
        Component[] components = GetComponents(typeof(AudioSource));
        foreach (var component in components)
        {
            _sources.Add((AudioSource)component);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!_paused)
        {
            // adjust audio values
            switch (_gunObject._weaponType)
            {
                case WeaponType.Pistol:
                {
                    _sources[0].pitch = 1.0f;
                    break;
                }
                case WeaponType.Rifle:
                {
                    _sources[0].pitch = 0.95f;
                    break;
                }
                case WeaponType.Heavy:
                {
                    _sources[0].pitch = 0.8f;
                    break;
                }
                case WeaponType.Sniper:
                {
                    _sources[0].pitch = 0.5f;
                    break;
                }
            }

            if (_health < (_maxHealth * HealthMulti))
            {
                _regenTimer -= Time.deltaTime * (_regenRate * HealthRegenRateMulti);

                if (_regenTimer <= 0.0f)
                {
                    _regenTimer = 0.0f;
                    _health += Time.deltaTime * _regenSpeed;
                    if (_health > (_maxHealth * HealthMulti))
                    {
                        _health = _maxHealth * HealthMulti;
                    }
                }
            }

            if (_moveForward || _moveBackward)
            {
                Vector3 move = _moveForward
                    ? ((_speed * MovementSpeedMulti) * Time.deltaTime * transform.forward)
                    : ((_speed * MovementSpeedMulti) / 1.5f * Time.deltaTime * -transform.forward);
                _characterController.Move(move);
            }

            if (_moveLeft || _moveRight)
            {
                Vector3 move = _moveLeft
                    ? ((_speed * MovementSpeedMulti) * Time.deltaTime * -transform.right)
                    : ((_speed * MovementSpeedMulti) / 1.5f * Time.deltaTime * transform.right);
                _characterController.Move(move);
            }

            if (_characterController.isGrounded && _velocity.y < 0)
            {
                _velocity.y = -2f;
            }

            if (_jump && _characterController.isGrounded)
            {
                _velocity.y = Mathf.Sqrt(_jumpHeight * -2f * _gravity);
            }

            if (_reload && _gunObject._currentAmmo != _gunObject._maxAmmo)
            {
                _gunObject._currentAmmo = 0;
            }


            _velocity.y += _gravity * Time.deltaTime;
            _characterController.Move(_velocity * Time.deltaTime);

            if (_swapTimer != 0.0f)
            {
                _swapTimer -= Time.deltaTime;
                _swapTimer = _swapTimer < 0.01f ? 0.0f : _swapTimer;
            }

            if (_swap && _swapTimer <= 0.0f)
            {
                _swapTimer = 1.0f;
                int currentWeapon = (int) _gunObject._weaponType;
                currentWeapon++;
                if (currentWeapon > 3)
                {
                    currentWeapon = 0;
                }

                _gunObject.gameObject.SetActive(false);
                _gunObject = transform.GetChild(0).GetChild(currentWeapon).gameObject.GetComponent<Weapon>();
                _gunObject.gameObject.SetActive(true);
                _gunObject.SwappedTo();
                _particleSystem = _gunObject.transform.GetChild(0).gameObject.GetComponent<ParticleSystem>();

            }


            if (!_gunObject.FullAuto)
            {
                if (Mouse.current.leftButton.wasPressedThisFrame && _shootTimer <= 0.0f)
                {
                    if (_gunObject._currentAmmo - 1 >= 0)
                    {
                        var success = _gunObject.Fire();
                        if (success)
                        {
                            _firing = true;
                            _particleSystem.Play();
                            _shootTimer = _gunObject.FireRate * Weapon.RoFMulti;
                            _sources[0].Play();
                        }
                    }
                }
                else
                {
                    _firing = false;
                }
            }
            else
            {
                if (Mouse.current.leftButton.IsPressed() && _shootTimer <= 0.0f)
                {
                    if (_gunObject._currentAmmo - 1 >= 0)
                    {
                        var success = _gunObject.Fire();
                        if (success)
                        {
                            _firing = true;
                            _particleSystem.Play();
                            _shootTimer = _gunObject.FireRate * Weapon.RoFMulti;
                            _sources[0].Play();
                        }
                    }
                }
                else
                {
                    _firing = false;
                }
            }

            _gunObject._animator.SetBool("Firing", _firing);

            if (_shootTimer != 0.0f)
            {
                _shootTimer -= Time.deltaTime * 2;
                _shootTimer = _shootTimer < 0.01f ? 0.0f : _shootTimer;
            }

            _healthBar.value = _health / (_maxHealth * HealthMulti);
        }
    }

    public void Reset()
    {
        transform.position = _startPos;
        _regenTimer = 3.0f;
        _health = _maxHealth * HealthMulti;
        _gunObject._currentAmmo = _gunObject._maxAmmo;
    }
    public void DamagePlayer(float damageValue)
    {
        _regenTimer = 3.0f;
        _health -= damageValue;
        _health = _health < 0 ? 0 : _health;
        Debug.Log("Ow!");
    }

    // Input Actions
    // W
    public void Forward(InputAction.CallbackContext context)
    {
        float value = context.ReadValue<float>();
        _moveForward = value > 0;
        //Debug.Log("Forward detected");
    }
    // S
    public void Backward(InputAction.CallbackContext context)
    {
        float value = context.ReadValue<float>();
        _moveBackward = value > 0;
        //Debug.Log("Backward detected");
    }
    // A
    public void Left(InputAction.CallbackContext context)
    {
        float value = context.ReadValue<float>();
        _moveLeft = value > 0;
        //Debug.Log("Forward detected");
    }
    // D
    public void Right(InputAction.CallbackContext context)
    {
        float value = context.ReadValue<float>();
        _moveRight = value > 0;
        //Debug.Log("Backward detected");
    }
    // Jump
    public void Jump(InputAction.CallbackContext context)
    {
        float button = context.ReadValue<float>();
        _jump = Mathf.Abs(button - 1.0f) < 0.1f;
        // Debug.Log("Interact detected: " + m_interact);
    }
    public void Shoot(InputAction.CallbackContext context)
    {
        float button = context.ReadValue<float>();
        _shoot = Mathf.Abs(button - 1.0f) < 0.1f;
        // Debug.Log("Interact detected: " + m_interact);
    }  
    public void Reload(InputAction.CallbackContext context)
    {
        float button = context.ReadValue<float>();
        _reload = Mathf.Abs(button - 1.0f) < 0.1f;
        // Debug.Log("Interact detected: " + m_interact);
    }
    public void SwapWeapons(InputAction.CallbackContext context)
    {
        float button = context.ReadValue<float>();
        _swap = Mathf.Abs(button - 1.0f) < 0.1f;
        // Debug.Log("Interact detected: " + m_interact);
    }
}


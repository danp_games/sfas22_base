using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    internal static float _encounterLevel = 1;

    private Board _board;
    private UI _ui;
    private double _gameStartTime;
    private bool _gameInProgress;

    internal static bool _clicked = false;
    public void OnClickedNewGame()
    {
        if (_board != null)
        {
            _board.RechargeBoxes();
        }

        if (_ui != null)
        {
            _ui.HideMenu();
            _ui.ShowGame();
        }

        //StartCoroutine(Starting());
    }

    public void OnClickedExit()
    {
#if !UNITY_EDITOR
        Application.Quit();
#endif
    }

    public void OnClickedReset()
    {
        if (_board != null)
        {
            _board.Clear();
        }

        if (_ui != null)
        {
            _ui.HideResult();
            _ui.ShowMenu();
        }
    }

    private void Awake()
    {
        _board = transform.parent.GetComponentInChildren<Board>();
        _ui = transform.parent.GetComponentInChildren<UI>();
        _gameInProgress = false;
    }

    private void Start()
    {
        if (_board != null)
        {
            _board.Setup(BoardEvent);
        }

        if (_ui != null)
        {
            _ui.ShowMenu();
        }
    }

    private void Update()
    {
        if(_ui != null)
        {
            _ui.UpdateTimer(_gameInProgress ? Time.realtimeSinceStartupAsDouble - _gameStartTime : 0.0);
        }

    }

    private void BoardEvent(Board.Event eventType)
    {
        //if(eventType == Board.Event.ClickedDanger && _ui != null)
        //{
        //    _ui.HideGame();
        //    _ui.ShowResult(success: false);
        //}

        //if (eventType == Board.Event.Win && _ui != null)
        //{
        //    _ui.HideGame();
        //    _ui.ShowResult(success: true);
        //}

        if (!_gameInProgress)
        {
            _gameInProgress = true;
            _gameStartTime = Time.realtimeSinceStartupAsDouble;
        }
    }
}

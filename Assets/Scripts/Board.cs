using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class Board : MonoBehaviour
{
    internal static Vector2 PlayerPosition = new Vector2(2, 0);
    internal static Vector2 GoalPosition = new Vector2(2, 4);
    
    public enum Event { ClickedBlank, ClickedNearDanger, ClickedDanger, Win, ClickedBoon };
    public enum Direction { Up, Down, Left, Right };

    [SerializeField] private Box BoxPrefab;
    [SerializeField] private int Width = 10;
    [SerializeField] private int Height = 10;
    [SerializeField] private int NumberOfDangerousBoxes = 10;
    [SerializeField] private ModeManager _modeManager;
    [SerializeField] private Image _playerMarker;
    [SerializeField] private GameObject _playerObject;
    [SerializeField] private Camera _camRef;

    internal Box[] _grid;
    private Vector2Int[] _neighbours;
    //private RectTransform _rect;
    private Transform _transform;
    private Action<Event> _clickEvent;
    private Player _playerRef;
    internal static bool _checked = false;
    private float inputTimer = 0.3f;
    public void Setup(Action<Event> onClickEvent)
    {
        _clickEvent = onClickEvent;
        Clear();
    }

    public void Clear()
    {
        for (int row = 0; row < Height; ++row)
        {
            for (int column = 0; column < Width; ++column)
            {
                int index = row * Width + (column % Height);
                _grid[index]?.StandDown();
            }
        }
    }


    private bool CheckDangerPositions(List<bool> dangerList)
    {
        for (int row = 0; row < Height; ++row)
        {
            for (int column = 0; column < Width; ++column)
            {
                int index = row * Width + (column % Height);
                if (_grid[index].RowIndex >= 2)
                {
                    if (dangerList[index])
                    {
                        return false; //failed check
                    }
                }
            }
        }

        return true;
    }

    public void RechargeBoxes()
    { 
        int numberOfItems = Width * Height;
        List<bool> dangerList = new List<bool>(numberOfItems);


        for (int count = 0; count < numberOfItems; ++count)
        {
            dangerList.Add(count < NumberOfDangerousBoxes);
            //dangerList.Add(false);
        }
        int maxX = 2;
        int maxY = Height;
        bool check = false;
        dangerList.RandomShuffle();
        //for (int i = 0; i < maxX; i++)
        //{
        //    for (int j = 0; j < maxY; j++)
        //    {
        //        int index = j * Width + i;
        //        if (dangerList[index])
        //        {
        //            dangerList[index] = false;
        //        }
        //    }
        //}
        dangerList[(int)(PlayerPosition.x * Width + PlayerPosition.y)] = false;

        int randomNo = 0;
        int defaultNo = NumberOfDangerousBoxes;
        bool pickedRandom = false;
        for (int row = 0; row < Height; ++row)
        {
            for (int column = 0; column < Width; ++column)
            {
                int index = row * Width + column;
                if (index == (int)(GoalPosition.x * Width + GoalPosition.y))
                {
                    _grid[index]?.Charge(CountDangerNearby(dangerList, index), dangerList[index], true, OnClickedBox);
                }
                else
                {
                    _grid[index]?.Charge(CountDangerNearby(dangerList, index), dangerList[index], false, OnClickedBox);
                }

                if (_grid[index].IsDangerous)
                {
                    randomNo = Random.Range(0,defaultNo);
                    if (randomNo == 0 && pickedRandom == false)
                    {
                        pickedRandom = true;
                        _grid[index].IsShop = true;
                        _grid[index].IsDangerous = false;
                        _grid[index].OnClick();
                        OnClickedBox(FindBoxWithPosition(new Vector2(row,column)));
                    }
                    else
                    {
                        defaultNo--;
                    }
                }
            }
        }


        _grid[(int)(PlayerPosition.x * Width + (PlayerPosition.y))].OnClick();
        OnClickedBox(FindBoxWithPosition(PlayerPosition));

        _grid[(int)(GoalPosition.x * Width + (GoalPosition.y))].OnClick();
        OnClickedBox(FindBoxWithPosition(GoalPosition));
    }

    private void Awake()
    {
        _grid = new Box[Width * Height];
        //_rect = transform as RectTransform;
        //RectTransform boxRect = BoxPrefab.transform as RectTransform;
        _transform = transform;
        //_rect.sizeDelta = new Vector2(boxRect.sizeDelta.x * Width, boxRect.sizeDelta.y * Height);
        Vector3 startPosition = new Vector3(0, 0, 0);
        startPosition.y *= -1.0f;

        startPosition.x += -100;
        startPosition.y += -124;

        _neighbours = new Vector2Int[8]
        {
            new Vector2Int(-Width - 1, -1),
            new Vector2Int(-Width, -1),
            new Vector2Int(-Width + 1, -1),
            new Vector2Int(-1, 0),
            new Vector2Int(1, 0),
            new Vector2Int(Width - 1, 1),
            new Vector2Int(Width, 1),
            new Vector2Int(Width + 1, 1)
        };
        int cumulativeX = 0;
        int cumulativeY = 0;
        int interval = 4;
        //for (int row = 0; row < Height; ++row)
        //{
        //    cumulativeY++;
        //    GameObject rowObj = new GameObject(string.Format("Row{0}", row));
        //    Transform rowRect = rowObj.transform;
        //    rowRect.SetParent(transform);
        //    //rowRect.anchoredPosition = new Vector2(_rect.anchoredPosition.x, startPosition.y - (boxRect.sizeDelta.y * row));
        //    //rowRect.sizeDelta = new Vector2(boxRect.sizeDelta.x * Width, boxRect.sizeDelta.y);
        //    rowRect.localScale = Vector2.one;

        //    for (int column = 0; column < Width; ++column)
        //    {
        //        int index = row * Width + column;
        //        _grid[index] = Instantiate(BoxPrefab, rowObj.transform);
        //        _grid[index].Setup(index, row, column);
        //        _grid[index]._position = new Vector2(row,column);
        //        //RectTransform gridBoxTransform = _grid[index].transform as RectTransform;
        //        _grid[index].name = string.Format("ID{0}, Row{1}, Column{2}", index, row, column);
        //        rowRect.position = new Vector3(startPosition.x + (interval * row), 0, startPosition.z + (interval * column));

        //        //gridBoxTransform.anchoredPosition = new Vector2( startPosition.x + (boxRect.sizeDelta.x * column), 0.0f);
        //    }
        //}
        GameObject gridHolder = new GameObject();
        gridHolder.name = "New Board";
        for (int row = 0; row < Height; ++row)
        {
            for (int column = 0; column < Width; ++column)
            {
                int index = row * Width + column;
                _grid[index] = Instantiate(BoxPrefab, gridHolder.transform);
                _grid[index].Setup(index, row, column);
                _grid[index]._position = new Vector2(row, column);
                _grid[index].transform.position = new Vector3(startPosition.x + (interval * row), 0, startPosition.z + (interval * column));

            }
        }

        //_grid[0].transform.GetChild(0).gameObject.GetComponent<Image>().color = Color.red;

        // Sanity check
        for (int count = 0; count < _grid.Length; ++count)
        {
            if (_grid[count] != null)
            {
                //Debug.LogFormat("Count: {0}  ID: {1}  Row: {2}  Column: {3}", count, _grid[count].ID,
                //    _grid[count].RowIndex, _grid[count].ColumnIndex);
            }
        }
        Box playerBox = FindBoxWithPosition(PlayerPosition);
        playerBox.IsPlayer = true;
        _playerObject = Instantiate(_playerObject);
        _playerObject.transform.position = playerBox.transform.position;
        _playerRef = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        _modeManager = GameObject.FindGameObjectWithTag("ModeManager").GetComponent<ModeManager>();
    }

    internal Box FindBoxWithPosition(Vector2 pos)
    {
        foreach (var box in _grid)
        {
            if (box._position == pos)
            {
                return box;
            }
        }

        return null;
    }

    void Update()
    {

        // Get the player box
        Box playerBox = FindBoxWithPosition(PlayerPosition);
        //_grid[0].Reveal();
        //Vector3 temp = playerBox.transform.GetChild(0).gameObject.GetComponent<Image>().transform.position;
        //_playerMarker.rectTransform.position =
        //    new Vector3(temp.x, temp.y, 0.0f);

        //Debug.Log(temp);
        Debug.Log("Values: "+ playerBox._position + " AND " + playerBox.ID);
        if (!_checked)
        {
            if (inputTimer > 0)
            {
                inputTimer -= Time.deltaTime;
            }

            if (Player._moveForward && inputTimer <= 0)
            {
                playerBox.IsPlayer = false;
                inputTimer = 0.3f;
                PlayerPosition.x--;
                if (PlayerPosition.x < 0)
                {
                    PlayerPosition.x = 0;
                }

                playerBox = FindBoxWithPosition(PlayerPosition);
                if (playerBox.Visited == false)
                {
                    playerBox.OnClick();
                    OnClickedBox(playerBox);
                }
                _playerObject.transform.position = playerBox.transform.position;
                playerBox.IsPlayer = true;

                //_grid[(int) (PlayerPosition.x * Width + PlayerPosition.y)].OnClick();
                //OnClickedBox(_grid[(int) (PlayerPosition.x * Width + PlayerPosition.y)]);
            }

            if (Player._moveBackward && inputTimer <= 0)
            {
                playerBox.IsPlayer = false;
                inputTimer = 0.3f;
                PlayerPosition.x++;
                if (PlayerPosition.x > Height - 1)
                {
                    PlayerPosition.x = Height - 1;
                }
                playerBox = FindBoxWithPosition(PlayerPosition);
                if (playerBox.Visited == false)
                {
                    playerBox.OnClick();
                    OnClickedBox(playerBox);
                }
                _playerObject.transform.position = playerBox.transform.position;
                playerBox.IsPlayer = true;
                //_grid[(int) (PlayerPosition.x * Width + PlayerPosition.y)].OnClick();
                //OnClickedBox(_grid[(int) (PlayerPosition.x * Width + PlayerPosition.y)]);
            }

            if (Player._moveLeft && inputTimer <= 0)
            {
                playerBox.IsPlayer = false;
                inputTimer = 0.3f;
                PlayerPosition.y--;
                if (PlayerPosition.y < 0)
                {
                    PlayerPosition.y = 0;
                }
                playerBox = FindBoxWithPosition(PlayerPosition);
                if (playerBox.Visited == false)
                {
                    playerBox.OnClick();
                    OnClickedBox(playerBox);
                }
                _playerObject.transform.position = playerBox.transform.position;
                playerBox.IsPlayer = true;
                //_grid[(int) (PlayerPosition.x * Width + PlayerPosition.y)].OnClick();
                //OnClickedBox(_grid[(int) (PlayerPosition.x * Width + PlayerPosition.y)]);
            }

            if (Player._moveRight && inputTimer <= 0)
            {
                playerBox.IsPlayer = false;
                inputTimer = 0.3f;
                PlayerPosition.y++;
                if (PlayerPosition.y > Width - 1)
                {
                    PlayerPosition.y = Width - 1;
                }
                playerBox = FindBoxWithPosition(PlayerPosition);
                if (playerBox.Visited == false)
                {
                    playerBox.OnClick();
                    OnClickedBox(playerBox);
                }
                _playerObject.transform.position = playerBox.transform.position;
                playerBox.IsPlayer = true;
                //_grid[(int) (PlayerPosition.x * Width + PlayerPosition.y)].OnClick();
                // OnClickedBox(_grid[(int) (PlayerPosition.x * Width + PlayerPosition.y)]);
            }
        }
        MovePlayer();
    }

    private void MovePlayer()
    {
        Vector2 newVec =
            new Vector2(
                Mathf.Lerp(_playerObject.transform.position.x, _camRef.transform.position.x, Time.deltaTime * 5),
                Mathf.Lerp(_playerObject.transform.position.z, _camRef.transform.position.z, Time.deltaTime * 5));
        _playerObject.transform.position = newVec;
    }

    private int CountDangerNearby(List<bool> danger, int index)
    {
        int result = 0;
        int boxRow = index / Width;

        //if (!danger[index])
        //{
            for (int count = 0; count < _neighbours.Length; ++count)
            {
                int neighbourIndex = index + _neighbours[count].x;
                int expectedRow = boxRow + _neighbours[count].y;
                int neighbourRow = neighbourIndex / Width;
                result += (expectedRow == neighbourRow && neighbourIndex >= 0 && neighbourIndex < danger.Count && danger[neighbourIndex]) ? 1 : 0;
            }
        //}

        return result;
    }

    private void OnClickedBox(Box box)
    {
        //Board.PlayerPosition = new Vector2(box._position.x, box._position.y);

        Event clickEvent = Event.ClickedBlank;

        if (box.IsDangerous)
        {
            clickEvent = Event.ClickedDanger;
            StartCoroutine(Starting());
            _checked = true;
        }
        else if (box.DangerNearby > 0)
        {
            clickEvent = Event.ClickedNearDanger;
        }

        if (box.IsShop)
        {
            clickEvent = Event.ClickedBoon;
        }

        ClearNearbyBlanks(box);

        if (CheckForWin())
        {
            clickEvent = Event.Win;
        }

        _clickEvent?.Invoke(clickEvent);

        if (!box.Visited)
        {
            box.SetVisited(true);
        }
    }
    private IEnumerator Starting()
    {
        ModeManager._enemyHealthMulti = 1.0f + (((Game._encounterLevel - 1) / 10));
        ModeManager._enemySpeedMulti = 1.0f;
        yield return new WaitForSeconds(1.0f);
        Debug.Log("Lets go!");
        ModeManager._mainCamRef.enabled = false;
        ModeManager._playerCamRef.enabled = true;
        ModeManager._mainCanvasRef.SetActive(false);
        ModeManager._playerCanvasRef.SetActive(true);
        _modeManager._boonUI.SetActive(false);
        _modeManager.Reset();
        _playerRef.Reset();
        ModeManager.Playing = true;
        Player._paused = false;
        Cursor.lockState = CursorLockMode.Locked;

    }

    private bool CheckForWin()
    {
        bool Result = true;

        for( int count = 0; Result && count < _grid.Length; ++count)
        {
            if (_grid[count] != null)
            {
                if (!_grid[count].IsDangerous && _grid[count].IsActive)
                {
                    Result = false;
                }
            }
        }

        return Result;
    }

    private void ClearNearbyBlanks(Box box)
    {
        RecursiveClearBlanks(box);
    }

    private void RecursiveClearBlanks(Box box)
    {
        if (!box.IsDangerous)
        {
            box.Reveal();

            if (box.DangerNearby == 0)
            {
                for (int count = 0; count < _neighbours.Length; ++count)
                {
                    int boxRow = box.ID / Width;
                    int neighbourIndex = box.ID + _neighbours[count].x;
                    int expectedRow = boxRow + _neighbours[count].y;
                    int neighbourRow = neighbourIndex / Width;
                    bool correctRow = expectedRow == neighbourRow;
                    bool active = neighbourIndex >= 0 && neighbourIndex < _grid.Length && _grid[neighbourIndex].IsActive;
                    if (correctRow && active)
                    {
                        _grid[neighbourIndex].Reveal();
                        RecursiveClearBlanks(_grid[neighbourIndex]);
                    }
                }
            }
        }
    }
}

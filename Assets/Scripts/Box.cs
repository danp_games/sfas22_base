using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Box : MonoBehaviour
{
    [SerializeField] private Color[] DangerColors = new Color[8];
    [SerializeField] private Image Danger;
    [SerializeField] private Image Goal;
    [SerializeField] private Image Shop;

    //private TMP_Text _textDisplay;
    private Image _image;
    private Button _button;
    private Action<Box> _changeCallback;
    public Vector2 _position;
    public int RowIndex { get; private set; }
    public int ColumnIndex { get; private set; }
    public int ID { get; private set; }
    public int DangerNearby { get; private set; }
    public bool IsDangerous { get; internal set; }
    public bool IsPlayer { get; internal set; }
    public bool IsShop { get; internal set; }
    public bool IsGoal { get; internal set; }
    public bool IsActive { get { return _button != null && _button.interactable; } }
    public bool Visited { get; private set; }
    

    public void Setup(int id, int row, int column)
    {
        ID = id;
        RowIndex = row;
        ColumnIndex = column;
        Goal.enabled = false;
        Shop.enabled = false;
        IsGoal = false;
        IsShop = false;
    }

    public void Charge(int dangerNearby, bool danger, bool goal, Action<Box> onChange)
    {
        _changeCallback = onChange;
        DangerNearby = dangerNearby;
        IsDangerous = danger && !goal;
        IsGoal = goal;
        IsShop = false;
        ResetState();
    }

    public void Reveal()
    {
        if (_button != null)
        {
            _button.interactable = false;
        }

        if (IsGoal && Goal != null)
        {
            Goal.enabled = true;
        }  
        
        if (IsShop && Shop != null)
        {
            Shop.enabled = true;
        }

        if (_image != null)
        {
            _image.enabled = true;
        }
    }

    public void StandDown()
    {
        if (_button != null)
        {
            _button.interactable = false;
        }

        if (Danger != null)
        {
            Danger.enabled = false;
        }

        if (_image != null)
        {
            _image.enabled = false;
        }
    }

    public void OnClick()
    {
        if (ColumnIndex <= Board.PlayerPosition.y + 1 &&
            ColumnIndex >= Board.PlayerPosition.y - 1 &&
            RowIndex <= Board.PlayerPosition.x + 1 &&
            RowIndex >= Board.PlayerPosition.x - 1 && 
            Game._clicked == false)
        {

            //Game._clicked = true;
            if (_button != null)
            {
                _button.interactable = false;
            }

            if (IsDangerous && Danger != null)
            {
                Danger.enabled = true;
            }
            else if (IsGoal && Goal != null)
            {
                Goal.enabled = true;
            } 
            else if (IsShop && Shop != null)
            {
                Shop.enabled = true;
            }
            else if (_image != null)
            {
                _image.enabled = true;
            }

            _changeCallback?.Invoke(this);
        }
    }

    internal void SetVisited(bool Visit)
    {
        Visited = Visit;
    }

    private void Awake()
    {
        //_button = GetComponent<Button>();
        //_image = _button.image;
        //_button.onClick.AddListener(OnClick);

        ResetState();
    }

    private void ResetState()
    {
        if (Danger != null)
        {
            Danger.enabled = false;
        }

        if (_image != null)
        {
            if (DangerNearby > 0)
            {
                _image.color = DangerColors[DangerNearby-1];
            }

            _image.enabled = false;
        }

        if (DangerNearby > 0)
        {
            Material mat = new Material(Shader.Find("Standard"));
            mat.color = DangerColors[DangerNearby - 1];
            transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().material = mat;
            transform.GetChild(1).gameObject.GetComponent<MeshRenderer>().material = mat;
            transform.GetChild(2).gameObject.GetComponent<MeshRenderer>().material = mat;
            transform.GetChild(3).gameObject.GetComponent<MeshRenderer>().material = mat;
            transform.GetChild(4).gameObject.GetComponent<MeshRenderer>().material = mat;
        }
        transform.GetChild(5).gameObject.SetActive(IsDangerous);

        if (_button != null)
        {
            _button.interactable = true;
        }
    }

    void Update()
    {
        transform.GetChild(6).gameObject.SetActive(IsPlayer);
    }
}
